from django.test import TestCase

from polls.utils import multiply_by_two


class TestUtils(TestCase):

    def test_multiply_by_two(self):
        self.assertTrue(multiply_by_two(2), 4)
