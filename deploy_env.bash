#!/bin/bash

RUN_LOCAL=false
BASE_DOMAIN="do.mionch.pl"
DROPLET_JUST_CREATED=false
STAGE=true
SUFFIX="stage"
YELLOW_COLOR='\033[1;33m'
WHITE_COLOR='\033[0m'
RED_COLOR='\033[031m'
SSH_KEYS_FINGERPRINT='c8:44:ce:0c:98:c9:19:44:79:01:b7:6b:3b:c0:6a:8b'

function config_environment_usage() {
    echo "usage: $(basename "${BASH_SOURCE[0]}") [options]"
    echo
    echo "options:"
    echo "  -h  --help              information about script usage"
    echo "  -c  --commit            deploy as new environment for commit"
    echo "  -s  --stage             deploy on stage"
    echo "  -l  --local             run environment from local machine"
    return 1
}

function check_result() {
    if [ "0" -ne "$?" ]; then
        echo "$1"
        exit 1
    fi
}

while [ "$#" -gt 0 ]; do
    case "$1" in
    "-h" | "--help")
        config_environment_usage
        exit
        ;;
    "-d" | "--default")       break ;;
    "-f" | "--commit")        if check_arg "$2"; then GIT_HASH="$2"; fi ;;
    "-s" | "--stage")         if check_arg "$2"; then STAGE="$2"; fi ;;
    "-l" | "--local")         RUN_LOCAL=true ;;
    esac
    shift
done

PROJECT_NAME_STAGE="${REPOSITORY_NAME}"
if [ "$RUN_LOCAL" == true ] ; then
  PROJECT_NAME=$(git config --local remote.origin.url)
  PROJECT_NAME=$( echo "$PROJECT_NAME" | sed -n 's#.*/\([^.]*\)\.git#\1#p')
  check_result "\n ${RED_COLOR}Project name not found. Please run this script inside git project.${WHITE_COLOR}"
fi

if [ ! $STAGE ]; then
  GIT_HASH=$(git rev-parse --short HEAD)
  PROJECT_NAME="$GIT_HASH-$PROJECT_NAME"
fi

# Check if droplet exist
doctl compute droplet get "$PROJECT_NAME"

if [ $? -ne '0' ]; then
  doctl compute droplet create --region fra1 --image ubuntu-18-04-x64 --size s-1vcpu-1gb --ssh-keys "$SSH_KEYS_FINGERPRINT" \
   "$PROJECT_NAME"
   sleep 10
fi

while [ "active" != "$(doctl compute droplet get "$PROJECT_NAME" --format Status --no-header)" ]; do
  echo "Waiting for droplet..."
  sleep 1
  DROPLET_JUST_CREATED=true
done

# Get the droplet public ip
SERVER_PUBLIC_IP=$(doctl compute droplet get "$PROJECT_NAME_STAGE" --format PublicIPv4 | tail -n 1)

if [ $? -ne '0' ]; then
  doctl compute domain records create "$BASE_DOMAIN" --record-type A --record-name "$PROJECT_NAME" --record-data "$SERVER_PUBLIC_IP"
fi

echo ALLOWED_HOSTS="$PROJECT_NAME.$BASE_DOMAIN" >> ./.env.docker

# Deploy docker containers
echo "Server public ip is: $SERVER_PUBLIC_IP"
if [ $DROPLET_JUST_CREATED == true ]; then
  echo "Droplet just created. Waiting for ssh..."
  while ! nc -vz "$SERVER_PUBLIC_IP" 22
  do
      echo "SSH daemon is not running. Trying again..."
      sleep 5
  done
fi

scp -o "StrictHostKeyChecking=no" -r ./.env.docker ./docker-compose.yml root@$"$SERVER_PUBLIC_IP":./
ssh -o "StrictHostKeyChecking=no" root@"$SERVER_PUBLIC_IP" << EOF

echo "Installing dependencies..."
sleep 5
apt update && apt install -y docker.io docker-compose gnupg2 pass

docker login ghcr.io -u "$DOCKER_OG_USER" -p "$DOCKER_OG_PASSWORD"

# Deploy docker images
docker-compose build
docker-compose up -d
docker image prune -af

echo "Deployment finished."
echo "You can access your project at : http://$PROJECT_NAME.$BASE_DOMAIN"
EOF

exit 0