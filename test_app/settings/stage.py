from test_app.settings.base import *

database = env('DJANGO_DATABASE', default='stage')

DATABASES = {
    'stage': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env('POSTGRES_NAME'),
        'USER': env('POSTGRES_USER'),
        'PASSWORD': env('POSTGRES_PASSWORD'),
        'HOST': env('POSTGRES_HOST'),
    }
}

SITE_URL = 'localhost:8000'
SITE_PROTOCOL = 'http'
META_SITE_PROTOCOL = SITE_PROTOCOL
BASE_URL = f'{SITE_PROTOCOL}://{SITE_URL}'
ALLOWED_HOSTS = env('ALLOWED_HOSTS', cast=list, default=[''])

DATABASES['default'] = DATABASES[database]
print('Using database: %s' % database)
