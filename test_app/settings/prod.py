from test_app.settings.base import *

database = env('DJANGO_DATABASE', default='prod')

DATABASES['default'] = DATABASES[database]

DATABASES = {
    'prod': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'raw_backend',
        'USER': 'raw_backend',
        'PASSWORD': '3wpEBks9Zb3HhX4V',
        'HOST': 'dev.ordergroup.pl',
        'TEST': {'NAME': 'raw_backend_test'},
    }
}

SITE_URL = 'localhost:8000'
SITE_PROTOCOL = 'http'
META_SITE_PROTOCOL = SITE_PROTOCOL
BASE_URL = f'{SITE_PROTOCOL}://{SITE_URL}'
ALLOWED_HOSTS = ['*']

print('Using database: %s' % database)