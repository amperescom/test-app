from test_app.settings.base import *

environ.Env.read_env(os.path.join(BASE_DIR, '.env.dev'))
database = env('DJANGO_DATABASE', default='dev')

DATABASES = {
    'dev': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env('POSTGRES_NAME'),
        'USER': env('POSTGRES_USER'),
        'PASSWORD': env('POSTGRES_PASSWORD'),
        'HOST': env('POSTGRES_HOST'),
    }
}

DATABASES['default'] = DATABASES[database]
print('Using database: %s' % database)
print('Using secret key: %s' % SECRET_KEY)

