#!/bin/sh

while ! nc -z $POSTGRES_HOST 5432; do
  echo "Waiting for postgres..."
  sleep 1
done

echo "PostgreSQL started"

echo "Applying database migrations..."
python manage.py migrate

echo "Collecting static files"
python manage.py collectstatic --noinput

python manage.py runserver 0.0.0.0:8000
