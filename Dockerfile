FROM python:3 as base
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
RUN apt update && apt install -y netcat && apt install -y iputils-ping

COPY . /code/

CMD ["ping", "-c", "1000", "localhost"]