#!/bin/bash
BACKED_IMAGE_NAME="ghcr.io/o-drobinin/test-app-backend"

function check_result() {
    if [ "0" -ne "$?" ]; then
        echo "$1"
        exit 1
    fi
}

function check_arg() {
    if [[ $1 == -* ]] || [ "$1" == "" ]; then config_environment_usage; else return 0; fi
}

function config_environment_usage() {
    echo "usage: $(basename "${BASH_SOURCE[0]}") [options]"
    echo
    echo "options:"
    echo "  -h  --help              information about script usage"
    echo "  -f  --file              Dockerfile"
    echo "  -t  --tag              build tag"
    return 1
}

if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    config_environment_usage
    exit
fi

while [ "$#" -gt 0 ]; do
    case "$1" in
    "-h" | "--help")
        config_environment_usage
        exit
        ;;
    "-d" | "--default")       break ;;
    "-f" | "--file")          if check_arg "$2"; then DOCKER_FILE="$2"; fi ;;
    "-t" | "--tag")           if check_arg "$2"; then BUILD_TAG="$2"; fi ;;
    esac
    shift
done


case "$(uname -s)" in
   Darwin)
     echo 'Mac OS X'
     sed -E -i '' "s/(.*-backend).*/\1$BUILD_TAG/" 'docker-compose.yml'
     ;;

   Linux)
     echo 'Linux'
     sed -E -i'' "s/(.*-backend).*/\1$BUILD_TAG/" 'docker-compose.yml'
     ;;

   *)
     echo 'Other OS'
     ;;
esac

DOCKER_BUILD_NAME="$BACKED_IMAGE_NAME:$BUILD_TAG"
(docker build -t "$DOCKER_BUILD_NAME" "$DOCKER_FILE")
check_result "Build problem"

docker push "$DOCKER_BUILD_NAME"
